const joi = require("@hapi/joi")

const { BadRequestError, InvalidArgumentError } = require("./errors")

// Schema Class
class Schema {
	constructor(schema) {
		try {
			// Intizialing Schema Object
			this.schema = joi.object().keys(schema)
		}
		catch (err) {
			throw err
		}
	}

	// Schema validation method
	async validate(payload) {
		try {
			if (payload == undefined) {
				// Payload empty
				throw new BadRequestError("Payload is Empty")
			}

			// Returning values
			return await this.schema.validateAsync(payload)
		}
		catch (err) {
			if (err.name == "ValidationError") {
				const errType = err.details[0].type
				const errParam = err.details[0].path[0]

				// Parameter missing validation error
				if (errType == "any.required") {
					throw new BadRequestError(err.message, errParam)
				}

				// Parameter not valid
				throw new InvalidArgumentError(err.message, errParam)
			}
			else {
				throw err
			}
		}
	}
}


module.exports = {
	Schema
}